﻿using UnityEngine;
using System.Collections;

public class GameController : MonoBehaviour {


	public GameObject hazard;
	public Vector3 spawnValues;
	public int hazardCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;

	public GUIText scoreText;
	public GUIText restartText;
	public GUIText gameOverText;
	private int score;

	private bool gameOver;
	private bool restart;


	public GameObject player;

	/*
	// pour que player Manager accede à Zones();
	public static GameController Instance;

	void Awake(){
		Instance = this;
	}
	//-------------------------------
	*/

	void Start () {
	

		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";

		score = 0;
		updateScore ();

		StartCoroutine(SpawnWaves ());

		Instantiate(player);
		Debug.Log("instantiate player");


		Debug.Log(Screen.width + "  " + Screen.height);

	}

	void Update(){
		if (restart) {

			if (Input.touchCount > 0) {
				Application.LoadLevel (Application.loadedLevel);
			}
			/*
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
			*/
		}

	}


	IEnumerator SpawnWaves () {


		yield return new WaitForSeconds (startWait);
		while (true) {
			for (int i = 0; i < hazardCount; i++) {
				Vector3 spawnPosition = new Vector3 (
					                        Random.Range (-spawnValues.x, spawnValues.x),
					                        spawnValues.y, 
					                        spawnValues.z);
				Quaternion spawnRotation = Quaternion.identity; // pas de rotation intiale
				Instantiate (hazard, spawnPosition, spawnRotation);
				yield return new WaitForSeconds (spawnWait);
			}
			yield return new WaitForSeconds (waveWait);
		
			if (gameOver) {
				restartText.text = "Clic to restart";
				restart = true;
				break;
			}
		
		}
	}


	void updateScore() {
		scoreText.text = "Score : " + score;
	}


	public void GameOver() {
		gameOverText.text = "Game Over!";
		gameOver = true;
	}

	// public methode
	public void AddScore(int newScoreValue) {
		score += newScoreValue;
		updateScore ();
	}


}