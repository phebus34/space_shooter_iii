﻿using UnityEngine;
using System.Collections;

/* attaché à l'astéroid */


public class DestroyByContact : MonoBehaviour {

	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;
	private GameController gameController;

	// on veur pouvoir accéder aux méthodes publiques de GameController
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController>();
		} else {
			Debug.Log ("script GameController non trouvé!");
		}
	}


	void OnTriggerEnter(Collider other) {
		
		 if (other.tag == "Boundary") {
			return;
		}

		Instantiate (explosion, transform.position, transform.rotation); // explose asteroid

		/*
		if (other.tag == "Player") {
			Instantiate (playerExplosion, other.transform.position, other.transform.rotation);  // axplode autre (vaisseasu du joueur)
			gameController.GameOver();
		
		}
		*/

		gameController.AddScore (scoreValue);
		//Destroy(other.gameObject); // destroy celui qui vient toucher (missile)
		//Destroy(gameObject); // destroy asteroid
	}
}
