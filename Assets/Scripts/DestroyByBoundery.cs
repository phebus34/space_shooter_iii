﻿using UnityEngine;
using System.Collections;

public class DestroyByBoundery : MonoBehaviour {


	private GameController gameController;


	// on veut pouvoir accéder aux méthodes publiques de GameController
	void Start () {
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController>();
		} else {
			Debug.Log ("script GameController non trouvé!");
		}
	}



	void OnTriggerExit(Collider other) {
		Destroy(other.gameObject);




		// si le vaisseaus sort de la zone, GameOver
		if (other.tag == "Player") {
			//Instantiate (playerExplosion, other.transform.position, other.transform.rotation);  // axplode autre (vaisseasu du joueur)
			gameController.GameOver();

		}



	}

}
